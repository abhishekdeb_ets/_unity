﻿// ***********************************************************************
// Assembly         : _unity
// Author           : Abhishek Deb
// Created          : 04-27-2015
//
// Last Modified By : Abhishek Deb
// Last Modified On : 04-27-2015
// ***********************************************************************
// <copyright file="Main.cs" company="Skipbits">
//     Copyright ©  2015
// </copyright>
// <summary>Contains Daily-use Unity Game classes and Libraries</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The _unity namespace.
/// Contains a lot of useful Classes for various everyday-use unity game development.
/// </summary>
namespace _unity
{
    /// <summary>
    /// A utility library containing lots of useful and everyday code helpers
    /// </summary>
    public static class _
    {

        #region Debug Helpers

        /// <summary>
        /// Enable Debug.log -> console.log Hoko [ Default : true ]
        /// </summary>
        public static bool enableWebDebug = true;

        /// <summary>
        /// Enable Debug at all? [ Default : true ]
        /// </summary>
        public static bool enableDebug = true;


        /// <summary>
        /// Once Set, Attaches a callback function for every Debug Print
        /// </summary>
        public static System.Action<string> DebugCallback;

        /// <summary>
        /// Outputs log with State time and stamp.
        /// </summary>
        /// <param name="msg">The data you want to dump to console.</param>
        /// <example>
        /// _.l("Hello");
        /// </example>
        public static void l(object msg = null)
        {
            string str = "@ " + DateTime.Now + " [" + Time.time + "] : " + msg.ToString();

            if (enableDebug)
            {
                //Actual Debug Info on Editor
                Debug.Log("LOG " + str);

                if (enableWebDebug)
                {
                    //Web PLayer Hook
                    if (Application.isWebPlayer)
                    {
                        Application.ExternalCall("print", str);
                    }
                }
            }

            //Debug Callback
            if (DebugCallback != null)
            {
                DebugCallback(str);
            }

        }

        #endregion Debug Helpers

        #region GUI Helpers

        /// <summary>
        /// Consistent GUI Scaling across all scenes
        /// </summary>
        /// <param name="customWidth">The width of the app</param>
        /// <param name="customHeight">The height of the app</param>
        /// <param name="Callback">The callback.</param>
        /// <example>
        /// void OnGUI(){ _.GUISetup(800f,480f); }
        /// </example>
        public static void GUISetup(float customWidth = 1366f, float customHeight = 768f, Action Callback = null)
        {
            GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(Screen.width / customWidth, Screen.height / customHeight, 1f));
            if (Callback != null) Callback();
        }

        #endregion GUI Helpers

        #region Array Helpers

        /// <summary>
        /// Initializes the array of objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="length">The length.</param>
        /// <returns>T[].</returns>
        public static T[] InitializeArrayObject<T>(int length) where T : new()
        {
            T[] array = new T[length];
            for (int i = 0; i < length; i++)
            {
                array[i] = new T();
            }
            return array;
        }

        #endregion Array Helpers

        #region Random Generators

        //Shuffles finites set ( )
        /// <summary>
        /// Shuffles the specified list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        public static void Shuffle<T>(this IList<T> list)
        {
            System.Random rng = new System.Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        #endregion Random Generators

        #region Extension Helpers
        /// <summary>
        /// Gets the or add component.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="child">The child.</param>
        /// <returns>T.</returns>
        static public T GetOrAddComponent<T>(this Component child) where T : Component
        {
            T result = child.GetComponent<T>();
            if (result == null)
            {
                result = child.gameObject.AddComponent<T>();
            }
            return result;
        }
        #endregion

    }
}