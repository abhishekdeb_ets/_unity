﻿using UnityEngine;

namespace _unity.Network
{
    public class Webplayer : Singleton<Webplayer>
    {
        public bool enableConsoleLog = true;
        public GameObject wp;


        /// <summary>
        /// Name of the proxy Class [ default: CallProxy]
        /// </summary>
        public object CallProxy;

        System.Action<object> Callback;


        public void Init(object CallProxyName=null)
        {
            wp = new GameObject("WebCom");
            wp.AddComponent<WebCom>();
            DontDestroyOnLoad(wp);

            CallProxy = CallProxyName;
            
            _.l("Webplayer Addon initialized");
        }

        public void Call(string JSMethod, System.Action<object> callback = null, params object[] param)
        {
            //_.l("[W]: Calling External JS Function '" + JSMethod + "'");
            Callback = callback;
            if(Application.isWebPlayer){
                Application.ExternalCall(JSMethod, param);
            }                
            else
            {
                _.l("[WM]: Calling Proxy . . .");
                CallProxy.GetType().GetMethod(JSMethod).Invoke(CallProxy, param);
            }
        }

        public void CallConfirm(object data)
        {
            //_.l("[W]: Acknowledgment received.");
            if (Callback != null)
                Callback(data);
        }
    }

    public class WebCom : MonoBehaviour
    {
        public void Call(object data)
        {
            //_.l("[WC]: Preparing Acknowledgment.");
            Webplayer.Instance.CallConfirm(data);
        }
    }


}
