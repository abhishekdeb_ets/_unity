﻿
// ***********************************************************************
// Assembly         : _unity
// Author           : Abhishek Deb
// Created          : 04-30-2015
//
// Last Modified By : Abhishek Deb
// Last Modified On : 04-30-2015
// ***********************************************************************
// <copyright file="StateMachine.cs" company="Skipbits">
//     Copyright ©  2015
// </copyright>
// <summary>Finite State Machine Implementation</summary>
// ***********************************************************************

using System;

/// <summary>
/// The StateMachine namespace.
/// </summary>
namespace _unity.StateMachine
{
    /// <summary>
    /// The Finite State Machine Class. A static class through which you define enums of state and pass it, along with the class to its Init method, and when needed, call the StateChange Method.
    /// </summary>
    /// <example>
    /// Out the following code in this file
    /// GameManager.cs 
    /// </example>
    /// <code>
    ///     using _unity;
    ///     using _unity.StateMachine;
    /// 
    ///     public class GameManager():Singleton<>{
    ///     
    ///         void Start()
    ///         {
    ///             FSM.init(this, States.init);
    ///         }
    ///         
    ///         // Simply define the methods with the same enum name
    ///         public void init(){
    ///             _.l("Im' initializing...");
    ///             
    ///             FSM.ChangeState(States.ready);
    ///         }
    ///         
    ///         // You can optionally provide a callback
    ///         public void ready(){
    ///             _.l("Im' ready now!");
    ///             
    ///             FSM.ChangeState(States.paused,delegate(){
    ///                 _.l("I'm done with being ready!");
    ///             });
    ///         }
    ///         
    ///         public void paused(){
    ///             _.l("Im' paused");
    ///             
    ///             Time.TimeScale=0;
    ///         }
    ///         
    ///     }
    ///     
    ///     public enum States{
    ///         init=0,
    ///         ready=1,
    ///         paused=2
    ///     }
    /// </code>
    public static class FSM
    {
        /// <summary>
        /// The object
        /// </summary>
        static Object Obj;
        /// <summary>
        /// The enm
        /// </summary>
        static Enum Enm;

        /// <summary>
        /// Initializes the specified object name.
        /// </summary>
        /// <param name="ObjectName">Name of the object.</param>
        /// <param name="eVal">The e value.</param>
        public static void Init(Object ObjectName, Enum eVal)
        {
            Obj = ObjectName;
            Enm = eVal;

            _.l("[FSM]: FSM Initialized for " + ObjectName.GetType().Name);

            ChangeState(Enm);
        }

        /// <summary>
        /// Changes the state.
        /// </summary>
        /// <param name="EnumState">State of the enum.</param>
        /// <param name="Callback">The callback.</param>
        public static void ChangeState(Object EnumState, System.Action Callback = null)
        {
            int id = (int)EnumState;
            _.l("[FSM]: State Change Initiated #" + id);
            foreach (var val in Enum.GetNames(Enm.GetType()))
            {
                if (val.ToString() == EnumState.ToString())
                {
                    try
                    {
                        Obj.GetType().GetMethod(EnumState.ToString()).Invoke(Obj, null);
                    }
                    catch (Exception e)
                    {
                        _.l("[_]: No Method for this state was found! Please define a method with the exact same name. (e): " + e.ToString());
                    }
                }
            }

            if (Callback != null)
            {
                _.l("[FSM]: Callback Initiated #" + id);
                Callback();
            }

            _.l("[FSM]: State Change Completed #" + id);
        }
    }
}
